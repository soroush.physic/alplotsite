#!/bin/bash

cd /opt/alplotsite

if [ "$DJANGO_SETUP" == "true" ]; then
  python setup.py install
fi
if [ "$DJANGO_MIGRATE" == "true" ]; then
  python manage.py migrate --noinput
fi
if [ "$DJANGO_COLLECTSTATIC" == "true" ]; then
  python manage.py collectstatic --noinput
fi

nginx

exec "$@"
