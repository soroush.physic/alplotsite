from django.shortcuts import reverse
from limitplots.models import LimitPlot, Visualization
from limitplots.tests import create_test_limitplot, create_test_visualization,\
                             create_formset_data, formset_to_dict
from records.models import Record
from records.tests import create_test_record
from users.tests import UsersTestCase


class LimitPlotNewTest(UsersTestCase):

    def setUp(self):
        super().setUp()
        self.client.force_login(self.alice)

        self.record = Record.objects.create(
            **create_test_record(creator=self.alice))

    def test_new_url_renders_correct_template(self):
        response = self.client.get(reverse("limitplot_new"))
        self.assertTemplateUsed(response, "limitplots/new.html")

    def test_new_view_has_empty_visualization_form(self):
        response = self.client.get(reverse("limitplot_new"))
        self.assertContains(response, "Color")
        self.assertContains(response, "Alpha")

    def test_new_view_allows_to_set_a_first_visualization_and_saves(self):
        formset_data = create_formset_data(
            models=[create_test_visualization(record=self.record.pk)])

        self.client.post(reverse("limitplot_new"),
                         data=dict(**create_test_limitplot(), **formset_data))

        self.assertEqual(LimitPlot.objects.count(), 1)
        self.assertEqual(Visualization.objects.count(), 1)

    def test_saving_two_visualization_works(self):
        formset_data = create_formset_data(models=[
            create_test_visualization(record=self.record.pk),
            create_test_visualization(record=self.record.pk),
        ])

        self.client.post(
            reverse("limitplot_new"),
            data=dict(**create_test_limitplot(), **formset_data)
        )

        self.assertEqual(LimitPlot.objects.count(), 1)
        self.assertEqual(Visualization.objects.count(), 2)

    def test_saving_two_visualizations_respects_order(self):
        formset_data = create_formset_data(models=[
            create_test_visualization(record=self.record.pk, ORDER=2),
            create_test_visualization(record=self.record.pk, ORDER=1),
        ])

        self.client.post(
            reverse("limitplot_new"),
            data=dict(**create_test_limitplot(), **formset_data)
        )

        self.assertEqual(Visualization.objects.get(pk=1).draw_order, 2)
        self.assertEqual(Visualization.objects.get(pk=2).draw_order, 1)

    def test_empty_visualization_record_field_is_ignored(self):
        formset_data = create_formset_data(models=[
            create_test_visualization(),
            create_test_visualization(record=self.record.pk),
        ])

        self.client.post(
            reverse("limitplot_new"),
            data=dict(**create_test_limitplot(), **formset_data)
        )

        self.assertEqual(LimitPlot.objects.count(), 1)
        self.assertEqual(Visualization.objects.count(), 1)

    def test_empty_visualization_alpha_field_shows_error(self):
        formset_data = create_formset_data(models=[
            create_test_visualization(record=self.record.pk, alpha=None),
        ])

        self.client.post(
            reverse("limitplot_new"),
            data=dict(**create_test_limitplot(), **formset_data)
        )

        self.assertEqual(LimitPlot.objects.count(), 0)
        self.assertEqual(Visualization.objects.count(), 0)

    def test_invalid_axis_range_shows_error(self):
        limitplot_data = create_test_limitplot(xrange_low=2, xrange_high=1)
        formset_data = create_formset_data(
            models=[create_test_visualization(record=self.record.pk)])

        response = self.client.post(
            reverse("limitplot_new"),
            data=dict(**limitplot_data, **formset_data)
        )

        self.assertContains(response, "xrange is invalid")

    def test_saving_a_limitplot_autosets_creator(self):
        formset_data = create_formset_data(models=[
            create_test_visualization(record=self.record.pk)
        ])

        self.client.post(reverse("limitplot_new"),
                         data=dict(**create_test_limitplot(), **formset_data))
        limitplot = LimitPlot.objects.first()

        self.assertEqual(limitplot.creator, self.alice)

    def test_view_can_show_preview(self):
        formset_data = create_formset_data(models=[
            create_test_visualization(record=self.record.pk)
        ])

        response = self.client.post(
            reverse("limitplot_new"),
            data=dict(
                **create_test_limitplot(),
                **formset_data,
                action="preview",
            )
        )

        self.assertIn("limitplot_preview", response.context)

    def test_user_group_has_permission_to_add_a_limitplot(self):
        with self.subTest("anonymous user gets redirected"):
            self.client.logout()
            response = self.client.get(reverse("limitplot_new"))
            self.assertRedirects(response, "{}?next={}".format(
                reverse("account_login"), reverse("limitplot_new")))

        with self.subTest("user in users group is allowed"):
            self.client.force_login(self.alice)
            response = self.client.get(reverse("record_new"))
            self.assertTrue(self.alice.has_perm("limitplots.add_limitplot"))
            self.assertEqual(response.status_code, 200)

    def test_creating_a_limitplot_sets_permissions_correct(self):
        formset_data = create_formset_data(
            models=[create_test_visualization(record=self.record.pk)])

        self.client.post(reverse("limitplot_new"),
                         data=dict(**create_test_limitplot(), **formset_data))

        limitplot = LimitPlot.objects.first()
        self.assertIsNotNone(limitplot)

        self.assertTrue(
            self.alice.has_perm("limitplots.view_limitplot", limitplot))
        self.assertTrue(
            self.alice.has_perm("limitplots.delete_limitplot", limitplot))
        self.assertTrue(
            self.alice.has_perm("limitplots.change_limitplot", limitplot))

    def test_can_only_select_records_user_has_permission_for(self):
        with self.subTest("bob cannot see alice's record"):
            self.client.force_login(self.bob)
            response = self.client.get(reverse("limitplot_new"))
            self.assertNotContains(response, self.record)

        with self.subTest("alice can see her record"):
            self.client.force_login(self.alice)
            response = self.client.get(reverse("limitplot_new"))
            self.assertContains(response, self.record)

    def test_user_cannot_manually_inject_pk_of_record_he_has_no_perm(self):
        self.assertFalse(self.bob.has_perm("records.view_record", self.record))

        formset_data = create_formset_data(
            models=[create_test_visualization(record=self.record.pk)])

        self.client.force_login(self.bob)
        self.client.post(reverse("limitplot_new"),
                         data=dict(**create_test_limitplot(), **formset_data))

        self.assertEqual(Visualization.objects.count(), 0)

    def test_can_start_limitplot_with_prefilled_first_record(self):
        response = self.client.get(
            reverse("limitplot_new"), {"initial": self.record.pk})
        # I know this assertion is ugly, but I do not know any better way
        self.assertContains(
            response, "<option value=\"{}\" selected>".format(self.record.pk))


class LimitPlotUpdateTest(UsersTestCase):

    def setUp(self):
        super().setUp()
        self.client.force_login(self.alice)

        self.limitplot = LimitPlot.objects.create(
            **create_test_limitplot(creator=self.alice))
        self.record = Record.objects.create(
            **create_test_record(creator=self.alice))

        self.visualization = Visualization(limitplot=self.limitplot,
                                           record=self.record,
                                           **create_test_visualization())
        self.visualization.save()

    def get_update_forms(self):
        # GET update view and parse form and formset for editing
        response = self.client.get(
            reverse("limitplot_update", args=[self.limitplot.pk]))
        form = response.context["form"].initial
        formset = formset_to_dict(response.context["formset"])
        return form, formset

    def test_edit_view_has_form_fields_prefilled(self):
        response = self.client.get(
            reverse("limitplot_update", args=[self.limitplot.pk]))

        self.assertContains(response, self.limitplot.name)
        self.assertContains(response, self.visualization.label)

    def test_edit_view_can_save_changes(self):
        form, formset = self.get_update_forms()
        form["name"] = "new name"
        formset["formset-0-label"] = "new label"

        self.client.post(
            reverse("limitplot_update", args=[self.limitplot.pk]),
            data=dict(**form, **formset),
        )
        self.assertEqual(LimitPlot.objects.first().name, "new name")
        self.assertEqual(Visualization.objects.first().label, "new label")

    def test_preview_does_not_submit(self):
        form, formset = self.get_update_forms()
        form["name"] = "new name"

        self.client.post(
            reverse("limitplot_update", args=[self.limitplot.pk]),
            data=dict(**form, **formset, action="preview"),
        )

        self.assertNotEqual(LimitPlot.objects.first().name, "new name")

    def test_view_can_show_preview(self):
        form, formset = self.get_update_forms()

        response = self.client.post(
            reverse("limitplot_update", args=[self.limitplot.pk]),
            data=dict(**form, **formset, action="preview"),
        )

        self.assertIn("limitplot_preview", response.context)

    def test_actually_stores_second_visualizations(self):
        form, formset = self.get_update_forms()
        formset.update(create_formset_data(
            total=2, initial=1,
            models=[
                create_test_visualization(record=self.record.pk, alpha=100),
                create_test_visualization(record=self.record.pk, alpha=90),
            ])
        )

        self.client.post(
            reverse("limitplot_update", args=[self.limitplot.pk]),
            data=dict(**form, **formset)
        )

        self.assertEqual(LimitPlot.objects.count(), 1)
        self.assertEqual(Visualization.objects.count(), 2)

    def test_can_delete_visualization(self):
        form, formset = self.get_update_forms()
        formset["formset-0-DELETE"] = True

        self.client.post(
            reverse("limitplot_update", args=[self.limitplot.pk]),
            data=dict(**form, **formset)
        )

        self.assertEqual(LimitPlot.objects.count(), 1)
        self.assertEqual(Visualization.objects.count(), 0)

    def test_saving_two_visualizations_respects_order(self):
        form, formset = self.get_update_forms()
        formset.update(create_formset_data(
            total=2, initial=1,
            models=[
                create_test_visualization(record=self.record.pk, ORDER=2),
                create_test_visualization(record=self.record.pk, ORDER=1),
            ])
        )

        self.client.post(
            reverse("limitplot_update", args=[self.limitplot.pk]),
            data=dict(**form, **formset)
        )

        self.assertEqual(Visualization.objects.get(pk=1).draw_order, 2)
        self.assertEqual(Visualization.objects.get(pk=2).draw_order, 1)

    def test_user_needs_permission_to_edit_limitplot(self):
        url = reverse("limitplot_update", args=[self.limitplot.pk])

        with self.subTest("bob is not allowed"):
            self.client.force_login(self.bob)
            response = self.client.get(url)
            self.assertEqual(response.status_code, 403)

        with self.subTest("alice can see her record"):
            self.client.force_login(self.alice)
            response = self.client.get(url)
            self.assertEqual(response.status_code, 200)


class LimitPlotDetailTest(UsersTestCase):

    def setUp(self):
        super().setUp()
        self.client.force_login(self.alice)

        self.limitplot = LimitPlot.objects.create(
            **create_test_limitplot(creator=self.alice))
        self.record = Record.objects.create(
            **create_test_record(creator=self.alice))

        self.visualization = Visualization(limitplot=self.limitplot,
                                           record=self.record,
                                           **create_test_visualization())
        self.visualization.save()

    def test_displays_basic_properties_in_correct_template(self):
        response = self.client.get(self.limitplot.get_absolute_url())

        self.assertTemplateUsed(response, "limitplots/detail.html")
        self.assertContains(response, self.limitplot.name)

    def test_user_can_only_view_limitplots_he_has_view_permission(self):
        with self.subTest("bob is not allowed"):
            self.client.force_login(self.bob)
            response = self.client.get(self.limitplot.get_absolute_url())
            self.assertEqual(response.status_code, 403)

        with self.subTest("alice can see her limitplot"):
            self.client.force_login(self.alice)
            response = self.client.get(self.limitplot.get_absolute_url())
            self.assertEqual(response.status_code, 200)


class LimitPlotDeleteTest(UsersTestCase):

    def setUp(self):
        super().setUp()
        self.client.force_login(self.alice)

        self.limitplot = LimitPlot.objects.create(
            **create_test_limitplot(creator=self.alice))
        self.record = Record.objects.create(
            **create_test_record(creator=self.alice))

        self.visualization = Visualization(limitplot=self.limitplot,
                                           record=self.record,
                                           **create_test_visualization())
        self.visualization.save()

    def test_renders_correct_template(self):
        response = self.client.get(
            reverse("limitplot_delete", args=[self.limitplot.pk]))
        self.assertTemplateUsed(response, "limitplots/delete.html")

    def test_confirmation_request_contains_name(self):
        response = self.client.get(
            reverse("limitplot_delete", args=[self.limitplot.pk]))

        self.assertContains(response, self.limitplot.name)

    def test_actually_deletes_limitplot_object(self):
        self.assertEquals(LimitPlot.objects.count(), 1)
        self.client.post(
            reverse("limitplot_delete", args=[self.limitplot.pk]))
        self.assertEquals(LimitPlot.objects.count(), 0)

    def test_deletes_related_visualizations_as_well(self):
        self.assertEquals(Visualization.objects.count(), 1)
        self.client.post(
            reverse("limitplot_delete", args=[self.limitplot.pk]))
        self.assertEquals(Visualization.objects.count(), 0)

    def test_user_needs_permission_to_delete_limitplot(self):
        url = reverse("limitplot_delete", kwargs={"pk": self.limitplot.pk})

        with self.subTest("bob is not allowed"):
            self.client.force_login(self.bob)
            response = self.client.post(url)
            self.assertEqual(response.status_code, 403)
            self.assertEqual(LimitPlot.objects.count(), 1)

        with self.subTest("alice can delete her limitplot"):
            self.client.force_login(self.alice)
            response = self.client.post(url)
            self.assertEqual(response.status_code, 302)
            self.assertEqual(LimitPlot.objects.count(), 0)


class LimitPlotListTest(UsersTestCase):

    def setUp(self):
        super().setUp()

        self.limitplot1 = LimitPlot.objects.create(
            **create_test_limitplot(creator=self.alice, name="Awesome plot"))
        self.limitplot2 = LimitPlot.objects.create(
            **create_test_limitplot(creator=self.bob, name="Not so nice plot"))

    def test_shows_only_items_user_has_permission_for(self):
        with self.subTest("bob see's his plot"):
            self.client.force_login(self.bob)
            response = self.client.get(reverse("limitplot_list"))
            self.assertNotContains(response, self.limitplot1.name)
            self.assertContains(response, self.limitplot2.name)

        with self.subTest("alice can see her limitplot"):
            self.client.force_login(self.alice)
            response = self.client.get(reverse("limitplot_list"))
            self.assertContains(response, self.limitplot1.name)
            self.assertNotContains(response, self.limitplot2.name)

    def test_search_results_only_list_plots_with_correct_permission(self):
        self.client.force_login(self.alice)
        response = self.client.get(
            reverse("limitplot_list"), {"query": "plot"})
        self.assertContains(response, self.limitplot1.name)
        self.assertNotContains(response, self.limitplot2.name)


class load_recordsTest(UsersTestCase):

    def setUp(self):
        super().setUp()
        self.client.force_login(self.alice)

        self.record1 = Record.objects.create(
            creator=self.alice,
            **create_test_record(title="Awesome Axion paper")
        )
        self.record2 = Record.objects.create(
            creator=self.bob,
            **create_test_record(title="Another Axion work")
        )

    def test_contains_records_with_correct_permission(self):
        response = self.client.get(
            reverse("ajax_load_records"), {"query": "aXiOn"})
        self.assertContains(response, self.record1.title)
        self.assertNotContains(response, self.record2.title)

    def test_contains_no_records_if_query_is_empty(self):
        response = self.client.get(
            reverse("ajax_load_records"), {"query": ""})
        self.assertNotContains(response, self.record1.title)
        self.assertNotContains(response, self.record2.title)


class download_as_pdf_Test(UsersTestCase):

    def setUp(self):
        super().setUp()
        self.client.force_login(self.alice)

        self.limitplot = LimitPlot.objects.create(
            **create_test_limitplot(creator=self.alice))

    def test_download_returns_HTTP_response(self):
        response = self.client.get(
            reverse("limitplot_pdf", args=[self.limitplot.pk]))
        self.assertEqual(response.status_code, 200)

    def test_cannot_download_limitplot_without_permission(self):
        self.client.force_login(self.bob)

        response = self.client.get(
            reverse("limitplot_pdf", args=[self.limitplot.pk]))
        self.assertEqual(response.status_code, 403)
