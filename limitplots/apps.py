from django.apps import AppConfig


class LimitplotsConfig(AppConfig):
    name = 'limitplots'
