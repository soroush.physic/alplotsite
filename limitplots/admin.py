from django.contrib import admin
from django.utils.safestring import mark_safe
from guardian.admin import GuardedModelAdmin
from limitplots.models import LimitPlot, Visualization


class VisualizationAdminInline(admin.TabularInline):
    model = Visualization
    extra = 0
    autocomplete_fields = ["record"]


@admin.register(LimitPlot)
class LimitPlotAdmin(GuardedModelAdmin):
    fieldsets = (
        (None, {
            "fields": ("name", "description",)
        }),
        (None, {
            "fields": (("creator", "creation_date", "last_update"),)
        }),
        ("Plot Options", {
            "classes": ("collapse",),
            "fields": (
                ("xrange_low", "xrange_high"),
                ("yrange_low", "yrange_high"),
                ("show_legend", "legend_loc"),
            ),
        }),
        ("Render", {
            "classes": ("collapse",),
            "fields": ("render", "image"),
        }),
    )

    inlines = [VisualizationAdminInline]

    list_display = ("name", "creator")
    list_filter = ("creator",)

    readonly_fields = ("creator", "creation_date", "last_update", "image")

    search_fields = ["name"]

    def image(self, obj):
        img_tag = "<img src=\"{}\" width=\"{}\" height=\"{}\" />".format(
            obj.render.url, obj.render.width, obj.render.height)
        return mark_safe(img_tag)
