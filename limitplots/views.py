from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.utils.text import slugify
from django.views.generic import CreateView, DetailView, UpdateView, \
                                 DeleteView, ListView
from guardian.decorators import permission_required_or_403
from guardian.mixins import PermissionListMixin, PermissionRequiredMixin
from guardian.shortcuts import get_objects_for_user
from limitplots.forms import LimitPlotForm, visualization_formset
from limitplots.models import LimitPlot
from listsearch.views import ListSearchMixin
from records.models import Record
from webplots.views import PreviewMixin


class LimitPlotFormsetBaseMixin(PermissionRequiredMixin, PreviewMixin):
    model = LimitPlot
    form_class = LimitPlotForm
    template_name = "limitplots/new.html"

    def form_valid(self, form):
        context = self.get_context_data()
        formset = context["formset"]

        if formset.is_valid():
            self.save_limitplot(form)

            for visualization in formset.save(commit=False):
                visualization.limitplot = self.object
                visualization.save()

            for v in formset.deleted_objects:
                v.delete()

            for form in formset.ordered_forms:
                if not form.cleaned_data["ORDER"]:
                    form.cleaned_data["ORDER"] = 0
                form.instance.draw_order = form.cleaned_data["ORDER"]
                form.instance.save()

            self.object.save(render=True)

            return redirect(self.object.get_absolute_url())
        else:
            return self.render_to_response(self.get_context_data())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if self.request.POST:
            context["form"] = LimitPlotForm(
                self.request.POST,
                instance=self.object)
            context["formset"] = visualization_formset(
                self.request.POST,
                instance=self.object,
                prefix="formset",
                form_kwargs={"user": self.request.user})
        else:
            context["form"] = LimitPlotForm(
                instance=self.object)
            context["formset"] = visualization_formset(
                instance=self.object,
                initial=[{"record": self.request.GET.get("initial")}],
                prefix="formset",
                form_kwargs={"user": self.request.user})

        return context

    def render_preview(self, context):
        form = context["form"]
        formset = context["formset"]

        form.is_valid()
        limitplot = form.save(commit=False)

        formset.is_valid()

        visualizations = [form.save(commit=False)
                          for form in formset.ordered_forms]

        return limitplot.draw(visualizations=visualizations)


class LimitPlotNew(LimitPlotFormsetBaseMixin, CreateView):
    permission_required = "limitplots.add_limitplot"
    permission_object = None
    preview_object = None

    def save_limitplot(self, form):
        self.object = form.save(commit=False)
        self.object.creator = self.request.user
        self.object.save(render=False)


class LimitPlotUpdate(LimitPlotFormsetBaseMixin, UpdateView):
    permission_required = "limitplots.change_limitplot"
    return_403 = True

    def save_limitplot(self, form):
        self.object.save()


class LimitPlotDetail(PermissionRequiredMixin, DetailView):
    model = LimitPlot
    template_name = "limitplots/detail.html"

    permission_required = "limitplots.view_limitplot"
    return_403 = True


class LimitPlotDelete(PermissionRequiredMixin, DeleteView):
    model = LimitPlot
    template_name = "limitplots/delete.html"
    success_url = reverse_lazy("limitplot_list")

    permission_required = "limitplots.delete_limitplot"
    return_403 = True


class LimitPlotList(PermissionListMixin, ListSearchMixin, ListView):
    model = LimitPlot
    context_object_name = "limitplots"
    template_name = "limitplots/list.html"

    permission_required = "limitplots.view_limitplot"

    def get_queryset(self, *args, **kwargs):
        queryset = super().get_queryset(*args, **kwargs)
        if self.query:
            return queryset.filter(name__icontains=self.query)
        return queryset


def load_records(request):
    queryset = Record.objects.none()

    query = request.GET.get("query")
    if query:
        queryset = get_objects_for_user(
            user=request.user,
            perms="records.view_record",
        ).search(query)

    return render(
        request, "limitplots/load_records.html", {"records": queryset})


@permission_required_or_403("limitplots.view_limitplot",
                            (LimitPlot, 'pk', 'pk'))
def download_as_pdf(request, pk):
    limitplot = LimitPlot.objects.get(pk=pk)
    response = HttpResponse(limitplot.render_to("pdf"),
                            content_type="application/pdf")
    response["Content-Disposition"] = "attachment; filename=\"{}.pdf\"".format(
        slugify(limitplot.name))
    return response
