from django.forms import BaseInlineFormSet
from django.forms import ModelForm
from django.forms.models import inlineformset_factory
from django.forms.formsets import DELETION_FIELD_NAME
from limitplots.models import LimitPlot, Visualization
from guardian.shortcuts import get_objects_for_user
from guardian.utils import get_anonymous_user


class LimitPlotForm(ModelForm):

    class Meta:
        model = LimitPlot
        exclude = [
            "records",
            "creator",
            "creation_date",
            "render",
            "last_update"
        ]


class VisualizationForm(ModelForm):

    def __init__(self, *args, **kwargs):
        user = kwargs.pop("user", get_anonymous_user())

        super().__init__(*args, **kwargs)

        self.fields["record"].queryset = get_objects_for_user(
            user=user, perms="records.view_record")

    class Meta:
        model = Visualization
        exclude = ()


class VisualizationInlineFormSet(BaseInlineFormSet):

    def _should_delete_form(self, form):
        delete_marked = form.cleaned_data.get(DELETION_FIELD_NAME, False)
        has_record = form.cleaned_data.get("record", False)
        return delete_marked or not has_record


visualization_formset = inlineformset_factory(
    parent_model=LimitPlot,
    model=LimitPlot.records.through,
    form=VisualizationForm,
    formset=VisualizationInlineFormSet,
    extra=1,
    can_order=True,
    can_delete=True,
)
