from django.conf.urls import url
from limitplots.views import LimitPlotNew, LimitPlotUpdate, LimitPlotDetail, \
                             LimitPlotDelete, LimitPlotList
from limitplots.views import load_records, download_as_pdf

urlpatterns = [
    url(r"^new$",
        LimitPlotNew.as_view(),
        name="limitplot_new"),

    url(r"^(?P<pk>\d+)/$",
        LimitPlotDetail.as_view(),
        name="limitplot_detail"),

    url(r"^(?P<pk>\d+)/edit$",
        LimitPlotUpdate.as_view(),
        name="limitplot_update"),

    url(r"^(?P<pk>\d+)/delete$",
        LimitPlotDelete.as_view(),
        name="limitplot_delete"),

    url(r"^list/$",
        LimitPlotList.as_view(),
        name="limitplot_list"),

    url(r"^ajax/load-records/",
        load_records,
        name="ajax_load_records"),

    url(r"^(?P<pk>\d+)/pdf$",
        download_as_pdf,
        name="limitplot_pdf"),
]
