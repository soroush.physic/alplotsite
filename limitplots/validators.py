from django.core.exceptions import ValidationError


def validate_finite_positive(value):
    if value <= 0:
        raise ValidationError("unphysical value", code="invalid")
