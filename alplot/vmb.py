import numpy as np
from alplot import units, photon_energy, ParameterSet


class VMBParameters(ParameterSet):

    defaults = {
        "wavelength": 1064.0 * units.nm,
        "delta_n": 4e-24 * units.T**-2,  # vacuum QED (unitary)
        "bfield": 1.0 * units.T,
        "length": 1.0 * units.meter,
    }


class VMBSetup(object):

    def __init__(self, **kwargs):
        self.pset = VMBParameters(**kwargs)

    def gagg(self, m):
        E = photon_energy(self.pset.wavelength)
        with units.context("hlu"):
            x = self.pset.length.to("eV**-1") * m**2 / (4. * E)
            red_sin = 1 - np.sin(2*x) / (2*x)
            dn = self.pset.delta_n * self.pset.bfield**2  # non-unitary
            g2 = 2 * dn * m**2 / (self.pset.bfield.to("eV**2")**2 * red_sin)

            return np.sqrt(g2.to("1/GeV**2"))
