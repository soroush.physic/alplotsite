from pint import Context, DimensionalityError, UnitRegistry


units = UnitRegistry()

hlu = Context("hlu")

hlu.add_transformation(
    "[mass]", "[energy]",
    lambda units, m: m * units.c**2
)

hlu.add_transformation(
    "[length]", "1/[energy]",
    lambda units, l: l / (units.c * units.hbar)
)

hlu.add_transformation(
    "[mass] / [current] / [time]**2", "[energy]**2",
    lambda units, B: B * (units.c**3 * units.hbar**3 / units.mu_0)**(1/2)
)

units.add_context(hlu)


def photon_energy(wavelength):
    E = units.c * units.h / wavelength
    return E.to("eV")


class ParameterSet(object):
    defaults = {}

    def __init__(self, **kwargs):
        # Set attributes ignoring keys not listed in `defaults`
        for key, value in self.defaults.items():
            if key in kwargs:
                setattr(self, key, kwargs[key])
            else:
                setattr(self, key, self.defaults[key])

    def __setattr__(self, name, value):
        if not isinstance(value, units.Quantity):
            value = units.Quantity(value)
        if value.dimensionality != self.defaults[name].dimensionality:
            raise DimensionalityError(value.dimensionality,
                                      self.defaults[name].dimensionality)
        super(ParameterSet, self).__setattr__(name, value)
