import numpy as np
from alplot import units, photon_energy, ParameterSet


class Cavity(ParameterSet):

    defaults = {
        "bfield": 1.0 * units.tesla,
        "length": 1.0 * units.meter,
        "magnets": 1 * units.dimensionless,
        "spacing": 0.0 * units.meter,
        "buildup": 1.0 * units.dimensionless,
        "refindex": 1.0 * units.dimensionless,
    }


class LSWParameters(ParameterSet):

    defaults = {
        "wavelength": 532.0 * units.nm,
        "power": 1.0 * units.W,
        "rate": 1.0 * units.Hz,
    }


class LSWSetup(object):

    def __init__(self, pc=None, rc=None, **kwargs):
        self.pc = pc if pc else Cavity()
        self.rc = rc if rc else Cavity()
        self.pset = LSWParameters(**kwargs)

    def prob_mod(self, m, cavity):
        cav = getattr(self, cavity)
        with units.context("hlu"):
            E = photon_energy(self.pset.wavelength)
            N = cav.magnets
            B = cav.bfield.to("eV**2")
            L = (cav.magnets * cav.length).to("eV**-1")
            s = cav.spacing.to("eV**-1")
            M2 = m**2 + 2 * E**2 * (cav.refindex - 1)

        return cav.buildup \
            * E / np.sqrt(E**2 - m**2) \
            * 4. * E**2 * B**2 / M2**2 \
            * np.sin(M2 * L / 4. / E / N)**2 \
            * np.sin(M2 * N * (L/N + s) / 4. / E)**2 \
            / np.sin(M2 * (L/N + s) / 4. / E)**2

    def gagg(self, m):
        E = photon_energy(self.pset.wavelength)
        g2 = np.sqrt(self.pset.rate * E / self.pset.power.to("eV/s")) /\
            np.sqrt(self.prob_mod(m, "pc") * self.prob_mod(m, "rc"))

        return np.sqrt(g2.to("1/GeV**2"))
