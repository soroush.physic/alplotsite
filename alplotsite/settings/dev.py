#!/usr/bin/env python
# encoding: utf-8

import os
from alplotsite.settings.base import *


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'on@oarsz9)t+l46l9_h_1kyjs3_ptrmw4qf!!3#n^f$6!shg-+'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Database
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Test Fixtures
FIXTURE_DIRS = [
    os.path.join(BASE_DIR, "fixtures"),
]
