from django.forms import ModelForm
from records.models import Record


class RecordForm(ModelForm):

    class Meta:
        model = Record
        exclude = ["creator", "creation_date", "render", "last_update"]
