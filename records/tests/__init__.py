def create_test_record(**kwargs):

    test_record = dict(
        title="New exclusion limits on scalar and pseudoscalar axionlike "
              "particles from light shining through a wall",
        abstract="Some fake abstract. THIS MIGHT BE LONG",
        author="OSQAR Collaboration",
        journal="Phys.Rev. D92 (2015) no.9, 092002",
        doi="10.1103/PhysRevD.92.092002",
        arxiv="arXiv:1506.08082",
        data="1,1\n10,10\n100,200",
        # creator, creation_date, last_edit are implicit
    )

    test_record.update(kwargs)

    return test_record
