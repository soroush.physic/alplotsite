from django.shortcuts import reverse
from records.models import Record
from records.tests import create_test_record
from users.tests import UsersTestCase
from limitplots.models import LimitPlot, Visualization
from limitplots.tests import create_test_limitplot, create_test_visualization


class RecordNewTest(UsersTestCase):

    def setUp(self):
        super().setUp()
        self.client.force_login(self.alice)

    def test_view_renders_the_correct_template(self):
        response = self.client.get(reverse("record_new"))
        self.assertTemplateUsed(response, "base.html")
        self.assertTemplateUsed(response, "records/new.html")

    def test_view_actually_creates_a_record(self):
        self.client.post(
            reverse("record_new"),
            data=dict(create_test_record())
        )

        saved_records = Record.objects.all()

        self.assertEqual(saved_records.count(), 1)
        for field, content in create_test_record().items():
            self.assertEqual(getattr(saved_records[0], field), content)

    def test_view_can_show_preview(self):
        response = self.client.post(
            reverse("record_new"),
            data=dict(data="1,2\n3,4", action="preview")
        )
        self.assertIn("record_preview", response.context)

    def test_view_only_shows_preview_for_valid_csv(self):
        response = self.client.post(
            reverse("record_new"),
            data=dict(data="not_valid_csv", action="preview")
        )
        # Just check for the img's alt tag as img checking is pointless
        self.assertNotIn("record_preview", response)

    def test_creating_a_record_autosets_the_creator(self):
        self.client.post(
            reverse("record_new"),
            data=dict(create_test_record())
        )
        record = Record.objects.all()[0]
        self.assertEqual(record.creator, self.alice)

    def test_user_must_have_permission_to_create_record(self):
        with self.subTest("anonymous user gets redirected"):
            self.client.logout()
            response = self.client.get(reverse("record_new"))
            self.assertRedirects(response, "{}?next={}".format(
                reverse("account_login"), reverse("record_new")))

        with self.subTest("user in users group is allowed"):
            self.client.force_login(self.alice)
            response = self.client.get(reverse("record_new"))
            self.assertTemplateUsed(response, "records/new.html")

    def test_creating_a_record_assigns_permissions(self):
        self.client.post(
            reverse("record_new"),
            data=dict(create_test_record())
        )
        record = Record.objects.all()[0]
        self.assertTrue(self.alice.has_perm("records.view_record", record))
        self.assertTrue(self.alice.has_perm("records.delete_record", record))

    def test_can_tag_a_new_record(self):
        self.client.post(
            reverse("record_new"),
            data=dict(create_test_record(tags="lsw"))
        )
        record = Record.objects.last()
        self.assertIn("lsw", record.tags.names())


class RecordDetailTest(UsersTestCase):

    def setUp(self):
        super().setUp()
        self.client.force_login(self.alice)

        self.test_record_fields = create_test_record()
        self.test_record = Record.objects.create(
            creator=self.alice, **self.test_record_fields)

    def test_view_contains_all_record_information(self):
        response = self.client.get(self.test_record.get_absolute_url())

        self.assertTemplateUsed(response, "records/detail.html")
        for field, content in self.test_record_fields.items():
            if field in ["data"]:
                continue
            self.assertContains(response, content)

    def test_user_can_only_view_records_he_has_permission(self):
        record_url = self.test_record.get_absolute_url()

        response = self.client.get(record_url)
        self.assertEqual(response.status_code, 200)

        self.client.force_login(self.bob)
        response = self.client.get(record_url)
        self.assertEqual(response.status_code, 403)


class RecordUpdateTest(UsersTestCase):

    def setUp(self):
        super().setUp()
        self.client.force_login(self.alice)

        self.test_record_fields = create_test_record()
        self.test_record = Record.objects.create(
            creator=self.alice, **self.test_record_fields)

    def test_view_uses_prepopulated_model_form(self):
        response = self.client.get(
            reverse("record_update", args=[self.test_record.pk]))
        self.assertContains(response, self.test_record.title)
        self.assertContains(response, self.test_record.author)

    def test_view_updates_the_model_object(self):
        response = self.client.get(
            reverse("record_update", args=[self.test_record.pk]))

        form_data = response.context["form"].initial
        form_data.update(title="A Changed Title")

        response = self.client.post(
            reverse("record_update", args=[self.test_record.pk]),
            data=dict(**form_data),
            follow=True,
        )
        self.assertContains(response, "A Changed Title")

    def test_view_can_update_preview_render(self):
        response = self.client.post(
            reverse("record_update", args=[self.test_record.pk]),
            data=dict(data="1,2\n3,4", action="preview")
        )
        self.assertIn("record_preview", response.context)

    def test_view_only_shows_preview_for_valid_csv(self):
        response = self.client.post(
            reverse("record_update", args=[self.test_record.pk]),
            data=dict(data="not_valid_csv", action="preview")
        )
        self.assertNotIn("record_preview", response.context)

    def test_user_can_only_edit_records_he_has_permission(self):
        response = self.client.get(
            reverse("record_update", args=[self.test_record.pk]))
        self.assertEqual(response.status_code, 200)

        self.client.force_login(self.bob)
        response = self.client.get(
            reverse("record_update", args=[self.test_record.pk]))
        self.assertEqual(response.status_code, 403)

    def test_can_remove_one_tag_and_add_the_other(self):
        self.test_record.tags.add("lsw")

        response = self.client.get(
            reverse("record_update", args=[self.test_record.pk]))
        self.assertContains(response, "lsw")

        form_data = response.context["form"].initial
        form_data.update(tags="helioscope")

        response = self.client.post(
            reverse("record_update", args=[self.test_record.pk]),
            data=dict(**form_data))
        self.assertIn("helioscope", self.test_record.tags.names())
        self.assertNotIn("lsw", self.test_record.tags.names())


class RecordDeleteTest(UsersTestCase):

    def setUp(self):
        super().setUp()
        self.client.force_login(self.alice)

        self.test_record_fields = create_test_record(creator=self.alice)
        self.test_record = Record.objects.create(**self.test_record_fields)

    def test_view_uses_correct_template(self):
        response = self.client.get(
            reverse("record_delete", args=[self.test_record.pk]))
        self.assertTemplateUsed(response, "records/delete.html")

    def test_view_actually_deletes_record(self):
        saved_records = Record.objects.all()

        self.assertEqual(saved_records.count(), 1)
        self.client.post(reverse("record_delete", args=[self.test_record.pk]))
        self.assertEqual(saved_records.count(), 0)

    def test_user_can_only_delete_records_he_has_permission(self):
        response = self.client.get(
            reverse("record_delete", args=[self.test_record.pk]))
        self.assertEqual(response.status_code, 200)

        self.client.force_login(self.bob)
        response = self.client.get(
            reverse("record_delete", args=[self.test_record.pk]))
        self.assertEqual(response.status_code, 403)

    def test_deletes_related_Visualizations_as_well(self):
        limitplot = LimitPlot.objects.create(
            **create_test_limitplot(creator=self.alice))

        visualization = Visualization(limitplot=limitplot,
                                      record=self.test_record,
                                      **create_test_visualization())
        visualization.save()

        self.assertEquals(Visualization.objects.count(), 1)
        self.client.post(reverse("record_delete", args=[self.test_record.pk]))
        self.assertEquals(Visualization.objects.count(), 0)


class RecordListTest(UsersTestCase):

    def setUp(self):
        super().setUp()
        self.client.force_login(self.alice)

        self.test_record_fields = create_test_record()
        self.test_record = Record.objects.create(
            creator=self.alice, **self.test_record_fields)

    def test_view_uses_correct_template(self):
        response = self.client.get(reverse("record_list"))
        self.assertTemplateUsed(response, "records/list.html")

    def test_view_contains_records(self):
        response = self.client.get(reverse("record_list"))
        self.assertContains(response, self.test_record.title)

    def test_list_shows_only_records_user_has_permission(self):
        response = self.client.get(reverse("record_list"))
        self.assertContains(response, self.test_record.title)

        self.client.force_login(self.bob)
        response = self.client.get(reverse("record_list"))
        self.assertNotContains(response, self.test_record.title)

    def test_filtered_list_only_shows_records_user_has_permission(self):
        record = Record.objects.create(
            creator=self.bob,
            **create_test_record(title="Not so good Axion paper"))
        response = self.client.get(
            reverse("record_list"), {"query": "Axion paper"})
        self.assertNotContains(response, record.title)


class download_as_csv_Test(UsersTestCase):

    def setUp(self):
        super().setUp()
        self.client.force_login(self.alice)

        self.test_record = Record.objects.create(
            creator=self.alice, **create_test_record())

    def test_download_returns_HTTP_response(self):
        response = self.client.get(
            reverse("record_csv", args=[self.test_record.pk]))
        self.assertEqual(response.status_code, 200)

    def test_downloaded_content_contains_csv(self):
        response = self.client.get(
            reverse("record_csv", args=[self.test_record.pk]))
        self.assertEqual(response.content.decode(response.charset),
                         self.test_record.data)

    def test_cannot_download_record_without_permission(self):
        self.client.force_login(self.bob)

        response = self.client.get(
            reverse("record_csv", args=[self.test_record.pk]))
        self.assertEqual(response.status_code, 403)
