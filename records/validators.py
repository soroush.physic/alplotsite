import numpy as np
import warnings
from django.core.exceptions import ValidationError
from io import StringIO


def validate_csv(raw):
    try:
        with warnings.catch_warnings(record=False):
            warnings.simplefilter("ignore")
            data = np.loadtxt(StringIO(raw), delimiter=",")
    except ValueError as e:
        raise ValidationError(str(e), code="invalid")

    if len(data.shape) <= 1:
        raise ValidationError("data is not in 2 columns", code="invalid")
    if data.shape[1] != 2:
        raise ValidationError("data is not in 2 columns", code="invalid")
