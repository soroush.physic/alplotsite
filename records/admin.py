from django.contrib import admin
from django.utils.safestring import mark_safe
from guardian.admin import GuardedModelAdmin
from records.models import Record


@admin.register(Record)
class RecordAdmin(GuardedModelAdmin):
    fieldsets = (
        (None, {
            "fields": ("title", "abstract", "author")
        }),
        (None, {
            "fields": (("creator", "creation_date", "last_update"),)
        }),
        ("References", {
            "classes": ("collapse",),
            "fields": ("journal", "doi", "arxiv"),
        }),
        ("Tags", {
            "classes": ("collapse",),
            "fields": ("tags",),
        }),
        ("Render", {
            "classes": ("collapse",),
            "fields": ("render", "image"),
        }),
        ("Data", {
            "classes": ("collapse",),
            "fields": ("data",),
        }),
    )

    list_display = ("title", "creator")
    list_filter = ("creator", "tags")

    readonly_fields = ("creator", "creation_date", "last_update", "image")

    search_fields = ["title"]

    def image(self, obj):
        img_tag = "<img src=\"{}\" width=\"{}\" height=\"{}\" />".format(
            obj.render.url, obj.render.width, obj.render.height)
        return mark_safe(img_tag)
