from django.conf.urls import url
from .views import RecordNew, RecordDetail, RecordUpdate, RecordDelete, \
                   RecordList, download_as_csv


urlpatterns = [
    url(r"^new$", RecordNew.as_view(), name="record_new"),
    url(r"^(?P<pk>\d+)/$", RecordDetail.as_view(), name="record_detail"),
    url(r"^(?P<pk>\d+)/edit$", RecordUpdate.as_view(), name="record_update"),
    url(r"^(?P<pk>\d+)/delete$", RecordDelete.as_view(), name="record_delete"),
    url(r"^(?P<pk>\d+)/csv$", download_as_csv, name="record_csv"),
    url(r"^list/$", RecordList.as_view(), name="record_list"),
]
