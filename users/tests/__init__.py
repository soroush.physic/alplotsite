from django.test import TestCase
from django.contrib.auth.models import User, Group


class UsersTestCase(TestCase):

    fixtures = [
        "groups.json",
        "testusers.json",
    ]

    def setUp(self):
        self.user_group = Group.objects.get(name="users")
        self.alice = User.objects.get(username="alice")
        self.bob = User.objects.get(username="bob")
