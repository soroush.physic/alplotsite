from django.contrib.auth.models import User
from django.urls import reverse
from users.tests import UsersTestCase


class TestLoggedInActions(UsersTestCase):

    def test_homepage_shows_login_link_if_not_logged_in(self):
        response = self.client.get(reverse("homepage"))
        self.assertContains(response, "Login")

    def test_homepage_shows_username_of_logged_in(self):
        self.client.force_login(self.alice)
        response = self.client.get(reverse("homepage"))
        self.assertContains(response, "alice")

    def test_homepage_shows_logout_link_if_logged_in(self):
        self.client.force_login(self.alice)
        response = self.client.get(reverse("homepage"))
        self.assertContains(response, "logout")

    def test_can_logout_a_logged_in_user(self):
        self.client.force_login(self.alice)
        response = self.client.get(reverse("account_logout"), follow=True)
        self.assertContains(response, "Login")

    def test_can_login_via_login_page(self):
        response = self.client.post(reverse("account_login"), data={
            "login": "alice", "password": "password123"}, follow=True)
        self.assertContains(response, "alice")

    def test_login_errors_are_shown(self):
        response = self.client.post(reverse("account_login"), data={
            "login": "foo", "password": "bar"}, follow=True)
        self.assertContains(
            response,
            "The username and/or password you specified are not correct."
        )

    def test_auto_add_registered_user_to_users_group(self):
        self.client.post(reverse("account_signup"), data={
            "username": "carol", "email": "carol@foo.de",
            "password1": "password123", "password2": "password123",
        })

        carol = User.objects.get(username="carol")

        self.assertIsNotNone(carol)
        self.assertIn(self.user_group, carol.groups.all())
