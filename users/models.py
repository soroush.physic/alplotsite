from django.contrib.auth.models import User, Group
from django.db import models
from django.dispatch import receiver


@receiver(models.signals.post_save, sender=User)
def user_post_save(sender, **kwargs):
    user, created = kwargs["instance"], kwargs["created"]
    user_group, _ = Group.objects.get_or_create(name="users")
    if created:
        user.groups.add(user_group)
